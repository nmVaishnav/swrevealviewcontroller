//
//  ViewController.m
//  SWRevealViewController
//
//  Created by Naman on 9/12/16.
//  Copyright © 2016 RGAP. All rights reserved.
//

#import "ViewController.h"
#import "SWRevealViewController.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.barButton.target = self.revealViewController;
//    self.barButton.action = @selector(revealToggle:);
//    
//    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    _barButton.target = self.revealViewController;
    _barButton.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
