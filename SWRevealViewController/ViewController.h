//
//  ViewController.h
//  SWRevealViewController
//
//  Created by Naman on 9/12/16.
//  Copyright © 2016 RGAP. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak,nonatomic) IBOutlet UIBarButtonItem *barButton;

@end

